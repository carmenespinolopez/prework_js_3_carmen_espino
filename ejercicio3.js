//Array de letras
var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

//Pedimos los datos necesarios, DNI y letra por separado
var numDni = prompt('Introduzca su NÚMERO de DNI (sin la letra, por favor)');
var letraDni = prompt('Introduzca la LETRA de su DNI.');

//Dividir el número del DNI / 23 para buscar la letra en la Array
function divisionDni (dni) {
    return numDni % 23;
}

//Comprobamos si la letra dada por el usuario es igual a la que sacamos de la división
function comprobarLetra(letra) {
    if (letraDni === letras[divisionDni()]) {
        return console.log('DNI válido, gracias.')
    } else {
        return console.log('DNI inválido, compruebe los datos introducidos, gracias.')
    }
}

//Comprobamos si el número es correcto. En caso afirmativo confirmamos que todo está ok
function comprobarDni(numDni) {
    if (numDni > 0 && numDni < 99999999) {
        return console.log(comprobarLetra(letraDni));
    }
    else {
        return console.log('El número de DNI facilitado no es correcto, por favor, compruébelo.')
    }
}

//Llamamos a la función
comprobarDni(numDni);
